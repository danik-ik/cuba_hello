alter table SAL_ORDER add constraint FK_SAL_ORDER_ON_CUSTOMER foreign key (CUSTOMER_ID) references SAL_CUSTOMER(ID);
create index IDX_SAL_ORDER_ON_CUSTOMER on SAL_ORDER (CUSTOMER_ID);
